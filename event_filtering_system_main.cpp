/* 
Author: Davide Garolini
Date: 11th August 2021


## An Event Filtering System
The goal is to create an event filtering system which can apply a given processing
function to a collection of events. Events consist of an ID and a time stamp.

Processing functions can:
- Transform event IDs and timestamps.
- Add events to the collection.
- Remove events to the collection based on a predicate.

The output of a filter is the entire collection after the processing function has been executed.
It should be possible to connect the output of a filter to the inputs of multiple other filters, 
the input of a filter could be a queue for example.
Filters should be able to run in separate threads.
 
Notes: 
    - IDs are generated sequentially in a unique form. Nonetheless there is an upper 
      limit of Events (max value of uint64_t) that can be created in this fashion.
    - multi-threading should be optimized according to the specific application
    - sorting functions for ids can be added
 */
#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <ctime>
#include <numeric>
#include <cmath>
#include <sstream>
#include <fstream> 
#include <algorithm>
#include <iterator>
#include <thread>

using namespace std;

// event structure
struct Event {
    static uint64_t nextID;
    uint32_t timestamp;
    uint64_t id;
    // constructor (the other ones are problematic with )
    Event(uint32_t ts) {
        this->timestamp = ts;
        this->id = ++nextID;
    }
    // Event(const Event& orig, uint32_t ts);
    // Event& operator=(const Event& orig);
    bool operator<(const Event& a) const{
        return timestamp < a.timestamp;
    }
};

// global init of static nextID
uint64_t Event::nextID = 0;

// different standard constructors
// Event::Event(const Event& orig) {
//     id = orig.id;
//     // timestamp = ts;
// }
// Event& Event::operator=(const Event& orig) {
//     id = orig.id;
//     return(*this);
// }

// function to print the vector of events vertically
void print_vEv_v(vector<Event> vEv_t);

// function to print the vector of events horizontally
void print_vEv_h(vector<Event> vEv_t);

// time generation function
vector<uint32_t> GenerateRandVec(int numOfNums, uint32_t min, uint32_t max);

// modify certain events
void filter_transform(vector<Event>& vEv_t, vector<uint64_t> id_t, function<void(Event&, uint32_t)> func,
                               uint32_t timeInit, uint32_t timeEnd);

// in the parallel version I did not succeded in using func as variable
void filter_transform_par(vector<Event>& vEv_t, vector<uint64_t> id_t,
                               uint32_t timeInit, uint32_t timeEnd);

// imaging that a new event happens, it will be assigned to another id and another timestamp
void new_id_event(Event & e, uint32_t new_ts);

// pop certain events
void filter_pop(vector<Event>& vEv_t, vector<uint64_t> id_t);

// TODO: add a pop for time based events

// add certain events
void filter_add(vector<Event>& vEv_t, int n_add, uint32_t timeInit, uint32_t timeEnd);

// initial simulator of events
void simulate_events(vector<Event>& vecEv, int nEv, uint32_t timeInit, uint32_t timeEnd);

// util for the parallel function (problem with passing by reference)
// void simulate_events_par(vector<vector<Event>>& vecEv, int wi, int nEv, uint32_t timeInit, uint32_t timeEnd);

// util that should be optimized and modified to be used with templates
vector<Event> flatten_events(vector<vector<Event>> v_vEv);

// util that should be optimized and modified to be used with templates 
vector<vector<Event>> split_events(vector<Event> vEv, int b);

template<typename T>
vector<T> slice(vector<T> const &v, int m, int n);

int main(){
    // creating first set of events with random event times and growing identification number
    cout << "Starting event simulation..." << endl;
    cout << endl;
    vector<Event> vecEv; // vector of Events

    int lVec = 6; // number of events
    uint32_t timeInit = time(NULL); // initial time in seconds
    
    // main simulation
    simulate_events(vecEv, lVec, timeInit, timeInit + 100);

    cout << "The following events have been generated:" << endl;
    print_vEv_h(vecEv);
    cout << endl;

    // transformation filter example: changing timestamp 
    // (id will be automatically changed by the constructor)
    vector<uint64_t> id_to_change;
    int pos_to_change[] = {3, 5};

    // determining the ids to use
    for(int i: pos_to_change){
        id_to_change.push_back(vecEv[i].id);
    }

    // filter-tranforming the events
    filter_transform(vecEv, id_to_change, new_id_event, timeInit + 100, timeInit + 200);
    // sort(vEv_t.begin(), vEv_t.end()); // sorting time of events // can be not necessary
    cout << "The events with id " << pos_to_change[0] << " and " << pos_to_change[1] << " have been modified:" << endl;
    print_vEv_h(vecEv);
    cout << endl;

    // popping events by their id
    int id_tp[] = {2, 3};
    vector<uint64_t> id_to_pop(id_tp, id_tp + sizeof(id_tp) / sizeof(id_tp[0]));
    filter_pop(vecEv, id_to_pop);
    cout << "The events with id " << id_tp[0] << " and " << id_tp[1] << " have been erased:" << endl;
    print_vEv_h(vecEv);
    cout << endl;

    // adding events by their id
    int n_add = 2;
    filter_add(vecEv, n_add, timeInit + 200, timeInit + 300);
    cout << "We added " << n_add << " events:" << endl;
    print_vEv_h(vecEv);
    cout << endl;

    // multi-threading -------------------------------------------------------------------------------
    vector<thread> threads;
    int batches = 8, n_ev_per_batch = 200;
    vector<vector<Event>> sim_Ev(batches); // for threads it needs to be initialized in size
    uint32_t time1 = 8, time2 = 68; // more readable timestamp ranges
    cout << "Starting multi-threading with " << batches << 
            " threads and " << n_ev_per_batch << " number of events per thread..." << endl;

    // simulating the events in parallel
    cout << "Simulating events..." << endl;
    for (int i = 0; i < batches; i++) {
        // threads.push_back(thread(simulate_events_par, ref(sim_Ev), i, 200, time1, time2));
        threads.push_back(thread(simulate_events, ref(sim_Ev[i]), n_ev_per_batch, time1, time2));
    }
    
    // joining the threads
    for (auto &th : threads) {
        th.join();
    }

    // flattening the vector for printing
    vector<Event> vEvents;
    vector<Event> vEv_to_print;
    vEvents = flatten_events(sim_Ev);
    sort(vEvents.begin(), vEvents.end());
    vEv_to_print = slice(vEvents, 190, 210);
    cout << "Printing values from position 190 to 210: " << endl;
    print_vEv_h(vEv_to_print);
    cout << endl;
    
    // changing the events in parallel
    cout << "Building again the batches and doing multi-threading..." << endl;
    vector<uint64_t> id_to_ch2 = {195, 198, 199, 18, 600, 700};
    time1 = 999;
    time2 = 1001;
    cout << "noo" << endl;
    // sim_Ev = split_events(vEvents, batches); // segmentation fault 11 (?) FAILS
    // print_vEv_h(sim_Ev[0]);

    for (int i = 0; i < batches; i++) {
        // threads.push_back(thread(filter_transform_par, ref(sim_Ev[i]), 
        // id_to_ch2, time1, time2)); // FAILS
        // libc++abi.dylib: terminating with uncaught exception of type std::__1::system_error: thread::join failed: Invalid argument
        // Abort trap: 6
        
        filter_transform_par(sim_Ev[i], id_to_ch2, time1, time2); // this works
    }
    
    // joining the threads
    // for (auto &th : threads) {
    //     th.join();
    // }

    // flattening the vector for printing
    vEvents = flatten_events(sim_Ev);
    sort(vEvents.begin(), vEvents.end());
    vEv_to_print = slice(vEvents, vEvents.size() - 10, vEvents.size() - 1);
    cout << "Printing values from position 190 to 210: " << endl;
    print_vEv_h(vEv_to_print);
    
    cout << endl;
    return 0;
}


// function declarations ---------------------------------------------------------------
// print the vector of events vertically
void print_vEv_v(vector<Event> vEv_t){
    for(auto vi: vEv_t){
        cout << "id: " << vi.id << " | time: " << vi.timestamp << endl;
    }
}
// print the vector of events horizontally
void print_vEv_h(vector<Event> vEv_t){
    cout << "id: {";
    int i = 1;
    for(auto vi: vEv_t){
        if(i < vEv_t.size()) 
            cout << vi.id << ", ";
        else
            cout << vi.id;
        i++;
    }
    cout << "}  | time: {";
    i = 1;
    for(auto vi: vEv_t){
        if(i < vEv_t.size()) 
            cout << vi.timestamp << ", ";
        else
            cout << vi.timestamp;
        i++;
    }
    cout << "}" << endl;
}

// simple random generator of numbers between max and min
vector<uint32_t> GenerateRandVec(int numOfNums,
        uint32_t min, uint32_t max){
    vector<uint32_t> vecValues;
    srand(time(NULL));
    int i = 0, randVal = 0;
    while(i < numOfNums){
        randVal = min + rand() % ((max + 1) - min);
        vecValues.push_back(randVal);
        i++;
    }
    return vecValues;
}

// filter to transform certain events with a for loop
void filter_transform(vector<Event>& vEv_t, vector<uint64_t> id_t, 
                      function<void(Event&, uint32_t)> func,
                      uint32_t timeInit, uint32_t timeEnd){
    vector<int> i_pos; // position in the vector of events
    int i = 0,j = 0; // loopers
    vector<uint32_t> new_timestamp;
    new_timestamp = GenerateRandVec(id_t.size(), timeInit, timeEnd);
    for(uint64_t id_t_i: id_t){
        i = 0;
        for(auto et: vEv_t){
            if(et.id == id_t_i){
                // vEv_t[i] = Event(5);
                // vEv_t[i].timestamp = 101;
                func(vEv_t[i], new_timestamp[j]);
                j++;
            }
            i++;
        }
    }
}

// tranforming an event, supposing it is a new one for a certain "slot"
void new_id_event(Event& e, uint32_t new_ts){
    e = Event(0); // new event, new id
    e.timestamp = new_ts; //new timing
}

// in the parallel version I did not succeded in using func as variable
void filter_transform_par(vector<Event>& vEv_t, vector<uint64_t> id_t,
                               uint32_t timeInit, uint32_t timeEnd){
    vector<int> i_pos; // position in the vector of events
    int i = 0,j = 0; // loopers
    vector<uint32_t> new_timestamp;
    new_timestamp = GenerateRandVec(id_t.size(), timeInit, timeEnd);
    for(uint64_t id_t_i: id_t){
        i = 0;
        for(auto et: vEv_t){
            if(et.id == id_t_i){
                // vEv_t[i] = Event(5);
                // vEv_t[i].timestamp = 101;
                vEv_t[i] = Event(0);
                vEv_t[i].timestamp = new_timestamp[j];
                j++;
            }
            i++;
        }
    }
}

// // pop certain events
void filter_pop(vector<Event>& vEv_t, vector<uint64_t> id_t){
    for(uint64_t id_ti: id_t){
        for(auto it = vEv_t.begin(); it != vEv_t.end(); it++){
            if ((*it).id == id_ti){
                vEv_t.erase(it--);
            }
        }
    }
}

// // add certain events
void filter_add(vector<Event>& vEv_t, int n_add, uint32_t timeInit, uint32_t timeEnd){
    vector<uint32_t> vecT; // vector of times

    vecT = GenerateRandVec(n_add, timeInit, timeEnd); // Event time generation
    sort(vecT.begin(), vecT.end()); // sorting time of events // can be unneccessary

    // main creation 
    for(int i = 0; i < n_add; i++){
        vEv_t.push_back(Event(vecT[i]));
    }    
}

// funtion to initialize the simulation
void simulate_events(vector<Event>& vecEv, int nEv, uint32_t timeInit, uint32_t timeEnd){
    vector<uint32_t> vecT; // vector of times

    vecT = GenerateRandVec(nEv, timeInit, timeEnd); // Event time generation
    sort(vecT.begin(), vecT.end()); // sorting time of events // can be unneccessary

    // main creation 
    for(int i = 0; i < nEv; i++){
        vecEv.push_back(Event(vecT[i]));
    }    
}
// extension for the reference seg fault
// void simulate_events_par(vector<vector<Event>>& vecEv, int wi, int nEv, uint32_t timeInit, uint32_t timeEnd){
//     simulate_events(vecEv[wi], nEv, timeInit, timeEnd);
// }

// util that should be optimized and modified to be used with templates
vector<Event> flatten_events(vector<vector<Event>> v_vEv){
    vector<Event> vEv;
     vEv = v_vEv[0];
    for(int i = 1; i < v_vEv.size(); i++){
        vEv.insert(end(vEv), begin(v_vEv[i]), end(v_vEv[i]));
    }
    return vEv;
}

// util that should be optimized and modified to be used with templates 
vector<vector<Event>> split_events(vector<Event> vEv, int b){
    cout << "mah";
    int l_stride = vEv.size() / b;
    cout << l_stride;
    vector<vector<Event>> v_vEv;
    for(int i = 0; i < b; i++){
        if(i < b - 1){
            // vector<Event> vEv_tmp(vEv.begin() + (i * l_stride), vEv.begin() + ((i + 1) * l_stride - 1));
            v_vEv[i] = slice(vEv, (i * l_stride), ((i + 1) * l_stride - 1));
        }else{
            v_vEv[i] = slice(vEv, (i * l_stride), vEv.size());
            // vector<Event> vEv_tmp(vEv.begin() + (i * l_stride), vEv.end());
        }
        // v_vEv[i] = vEv_tmp;
    }
    return v_vEv;
}

// util for slicing
template<typename T>
vector<T> slice(vector<T> const &v, int m, int n){
    auto first = v.cbegin() + m;
    auto last = v.cbegin() + n + 1;
 
    vector<T> vec(first, last);
    return vec;
}
